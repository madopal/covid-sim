#!/usr/bin/env python

import os
import sys
from argparse import ArgumentParser
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource, Label, LabelSet, Range1d
import pprint
import json
from collections import defaultdict
from statistics import mean, median
import operator

def parse_cmd_args():
    """ Routine to setup argparse """
    parser = ArgumentParser()
    parser.add_argument('-d', '--data-file',
                        help='file with simulation data',
                        required=True)
    parser.add_argument('-c', '--count',
                        type=int,
                        help='how many runs to process')
    args = parser.parse_args()
    return args

def graph_chamber_situations(
    chamber_data=None,
    graph_title=None,
    x_axis_label=None,
    y_axis_label=None,
    legend_label="voting distribution",
    bar_color='green',
    stroke_color='black',
    file_name=None):
    
    plot_width=800
    plot_height=600
    for run in chamber_data:
        for chamber, ch_data in run['chamber_stats']:
            for party, pa_data in ch_data['party_stats']:
                print
    sys.exit()
    # output to static HTML file
    output_file(file_name)
    # create a new plot with a title and axis labels
    p = figure(
        plot_width=plot_width,
        plot_height=plot_height,
        title=graph_title,
        x_axis_label=x_axis_label,
        y_axis_label=y_axis_label)

    #for party, party_data in chamber_stats['party_stats']:
        

    #source = ColumnDataSource(data=dict(
    #    height=chamber_data,
    #    weight=[x for x in range(0, len(chamberdata))],
    #    labels=chamber_data))
    
    #labels = LabelSet(x='height', y='weight', text='labels', level='overlay',
    #              x_offset=-20, y_offset=-7, text_align='right',
    #              text_font_size='8pt',text_color='white',
    #              source=source, render_mode='canvas')

    # add a line renderer with legend and line thickness
    p.hbar_stack(
        [party1, hosp_party1, party2, hosp_party2],
        y=indexes,
        height=1, 
        color=[party1_color, hosp_party1_color, party2_color, hosp_party2_color],
        line_color=stroke_color,
        line_width=2,
        source=source)
    p.y_range.flipped = True
    p.add_layout(labels)
    show(p)

def graph_member_fatality_frequency(
    sorted_member_data=None,
    full_member_data=None,
    graph_title=None,
    x_axis_label=None,
    y_axis_label=None,
    legend_label="fatality freqency",
    bar_color='green',
    stroke_color='black',
    file_name=None):

    plot_width=800
    plot_height=600
    print('{} members passed'.format(len(sorted_member_data)))
    # output to static HTML file
    output_file(file_name)
    # create a new plot with a title and axis labels
    p = figure(
        plot_width=plot_width,
        plot_height=plot_height,
        title=graph_title,
        x_axis_label=x_axis_label,
        y_axis_label=y_axis_label)

    source = ColumnDataSource(data=dict(
        height=[x[1] for x in sorted_member_data],
        weight=[x for x in range(0, len(sorted_member_data))],
        labels=[x[0] for x in sorted_member_data]))
    
    labels = LabelSet(x='height', y='weight', text='labels', level='overlay',
                  x_offset=-20, y_offset=-7, text_align='right',
                  text_font_size='8pt',text_color='white',
                  source=source, render_mode='canvas')

    # add a line renderer with legend and line thickness
    p.hbar(
        y='weight',
        right='height',
        height=1, 
        color=bar_color,
        line_color=stroke_color,
        line_width=2,
        source=source)
    p.y_range.flipped = True
    p.add_layout(labels)
    show(p)


def graph_fatality_frequency(
    data_set=None,
    sorted_fatality_counts=None,
    sorted_fatalities=None,
    graph_title=None,
    x_axis_label=None,
    y_axis_label=None,
    legend_label="fatality freqency",
    mean_citation_text=None,
    median_citation_text=None,
    bar_color='green',
    stroke_color='black',
    file_name=None):

    plot_width=1200
    plot_height=600

    # output to static HTML file
    output_file(file_name)
    # create a new plot with a title and axis labels
    p = figure(
        plot_width=plot_width,
        plot_height=plot_height,
        title=graph_title,
        x_axis_label=x_axis_label,
        y_axis_label=y_axis_label)

    source = ColumnDataSource(data=dict(
        height=sorted_fatality_counts,
        weight=sorted_fatalities,
        labels=sorted_fatality_counts))

    labels = LabelSet(x='weight', y='height', text='labels', level='glyph',
                  x_offset=-12, y_offset=0, source=source, render_mode='canvas')
    mean_citation = Label(
        x=plot_width-215,y=plot_height-150,
        x_units='screen', y_units='screen',
        text_font_size='10pt',
        text=mean_citation_text, render_mode='css',
        background_fill_color='white', background_fill_alpha=1.0)
    median_citation = Label(
        x=plot_width-215, y=plot_height-170,
        x_units='screen', y_units='screen',
        text_font_size='10pt',
        text=median_citation_text, render_mode='css',
        background_fill_color='white', background_fill_alpha=1.0)
    # add a line renderer with legend and line thickness
    p.vbar(
        x='weight',
        top='height',
        legend_label=legend_label,
        width=0.5, 
        color=bar_color,
        line_color=stroke_color,
        line_width=2,
        source=source)

    p.add_layout(labels)
    p.add_layout(mean_citation)
    p.add_layout(median_citation)
    show(p)

def find_congressperson_frequency(data_set=None, max_count=None):
    congress_freq = {}
    summaries = []
    count = 0
    for entry in data_set:
        count += 1
        sys.stdout.write('Processing run {:06d}\r'.format(count))
        sys.stdout.flush()
        summaries.append(entry['summary_data'])
        for office in entry['run_data']:
            name = office['name']
            d_data = entry['demog_data']
            if name not in congress_freq:
                congress_freq[name] = {}
                congress_freq[name]['party'] = d_data[name]['party']
                congress_freq[name]['chamber'] = d_data[name]['chamber']
                congress_freq[name]['age'] = d_data[name]['age']
                congress_freq[name]['state'] = d_data[name]['state']
                congress_freq[name]['birthday'] = d_data[name]['birthday']
                congress_freq[name]['death_count'] = 0
                congress_freq[name]['hospitalized_count'] = 0
                congress_freq[name]['icu_count'] = 0
                congress_freq[name]['infected_count'] = 0
            if not office['alive']:
                congress_freq[office['name']]['death_count'] += 1
            if office['hospitalized'] == True:
                congress_freq[office['name']]['hospitalized_count'] += 1
            if office['in_icu'] == True:
                congress_freq[office['name']]['icu_count'] += 1
            if office['infected'] == True:
                congress_freq[office['name']]['infected_count'] += 1
                
            if max_count:
                if count >= max_count:
                    break
   
    for member, data in congress_freq.items():
        congress_freq[member]['death_rate'] =\
            float(congress_freq[member]['death_count']) /\
            float(len(data_set)) * 100.0
        congress_freq[member]['hospitalized_rate'] =\
            float(congress_freq[member]['hospitalized_count']) /\
            float(len(data_set)) * 100.0
        congress_freq[member]['icu_rate'] =\
            float(congress_freq[member]['icu_count']) /\
            float(len(data_set)) * 100.0
        congress_freq[member]['infected_rate'] =\
            float(congress_freq[member]['infected_count']) /\
            float(len(data_set)) * 100.0

    return congress_freq, summaries

def main():
    args = parse_cmd_args()

    print('Loading data from {}'.format(args.data_file))
    with open(args.data_file, 'r') as in_file:
        sim_data = json.load(in_file)

    print('{} sims found'.format(len(sim_data)))
    congress_freq, summaries = find_congressperson_frequency(
        data_set=sim_data, max_count=args.count)
    member_fatality_dict = {}
    for member, data in congress_freq.items():
        member_fatality_dict[member] = data['death_count']

    sorted_members = sorted(
        member_fatality_dict.items(),
        key=operator.itemgetter(1),
        reverse=True)
    count = 25
    highest_fatality = defaultdict(int)
    graph_member_data = []
    for member in sorted_members:
        graph_member_data.append(member)
        member_name = member[0]
        highest_fatality[congress_freq[member_name]['party']] += 1
        #print('{} {} {} - fatalities: {}, fatality rate: {:0.2f}%'.format(
        #    member_name,
        #    congress_freq[member_name]['party'],
        #    congress_freq[member_name]['chamber'],
        #    congress_freq[member_name]['death_count'],
        #    congress_freq[member_name]['death_rate']))
        count -= 1
        if count <= 0:
            break

    #graph_chamber_situations(
    #    chamber_data=summaries,
    #    file_name='top_member_fatalties1.html',
    #    x_axis_label='fatalities per {} simulations'.format(len(sim_data)),
    #    y_axis_label='Congress members',
    #    bar_color='purple')
    #sys.exit()

    graph_member_fatality_frequency(
        file_name='top_member_fatalties1.html',
        x_axis_label='fatalities per {} simulations'.format(len(sim_data)),
        y_axis_label='Congress members',
        bar_color='purple',
        sorted_member_data=graph_member_data,
        full_member_data=congress_freq)

    fatality_counts = []
    run_ids = []
    party_fatality_counts = {}
    chamber_fatality_counts = {}
    fatality_frequency = defaultdict(int)
    party_fatality_frequency = {}
    chamber_fatality_frequency = {}
    index = 0
    for entry in sim_data:
        summary_data = entry.get('summary_data')
        if 'dead' in summary_data:
            cur_field = 'dead'
        else:
            cur_field = 'fatalities'
        fatality_frequency[summary_data.get(cur_field)] += 1
        fatality_counts.append(summary_data.get(cur_field))
        run_ids.append(index)
        index += 1
        for party, party_data in summary_data.get('party_stats').items():
            if party not in party_fatality_counts:
                party_fatality_counts[party] = []
            if party not in party_fatality_frequency:
                party_fatality_frequency[party] = defaultdict(int)
            party_fatality_counts[party].append(party_data.get(cur_field))
            party_fatality_frequency[party][party_data.get(cur_field)] += 1
        for chamber, chamber_data in summary_data.get('chamber_stats').items():
            if chamber not in chamber_fatality_counts:
                chamber_fatality_counts[chamber] = []
            if chamber not in chamber_fatality_frequency:
                chamber_fatality_frequency[chamber] = defaultdict(int)
            chamber_fatality_counts[chamber].append(chamber_data.get(cur_field))
            chamber_fatality_frequency[chamber][chamber_data.get(cur_field)] += 1

    sorted_fatalities = sorted(list(fatality_frequency.keys()))
    sorted_fatality_counts = []
    for death_num in sorted_fatalities:
        sorted_fatality_counts.append(fatality_frequency[death_num])

    graph_fatality_frequency(
        data_set=sim_data,
        sorted_fatality_counts=sorted_fatality_counts,
        sorted_fatalities=sorted_fatalities,
        x_axis_label='# Congressional fatalities',
        y_axis_label='frequency of occurrence',
        mean_citation_text='mean fatalities: {}'.format(mean(fatality_counts)),
        median_citation_text='median fatalities: {}'.format(median(fatality_counts)),
        legend_label='fatality frequency',
        bar_color='purple',
        graph_title='COVID-19 Congressional Fatality Plot ({} sims)'.format(len(sim_data)),
        file_name='total_fatalities1.html')

    for party, data in party_fatality_frequency.items():
        sorted_fatalities = sorted(list(data.keys()))
        sorted_fatality_counts = []
        for death_num in sorted_fatalities:
            sorted_fatality_counts.append(data[death_num])

        if 'repub' in party.lower():
            bar_color = 'red'
        elif 'dem' in party.lower():
            bar_color = 'blue'
        else:
            bar_color = 'green'

        graph_fatality_frequency(
            data_set=sim_data,
            sorted_fatality_counts=sorted_fatality_counts,
            sorted_fatalities=sorted_fatalities,
            x_axis_label='# {} fatalities'.format(party),
            y_axis_label='frequency of occurrence',
            mean_citation_text='mean fatalities: {}'.format(
                mean(party_fatality_counts[party])),
            median_citation_text='median fatalities: {}'.format(
                median(party_fatality_counts[party])),
            bar_color=bar_color,
            graph_title=('COVID-19 {} Congressional Fatality Plot ({} sims)'
                .format(party, len(sim_data))),
            file_name='total_{}_fatalities1.html'.format(party))

    for chamber, data in chamber_fatality_frequency.items():
        sorted_fatalities = sorted(list(data.keys()))
        sorted_fatality_counts = []
        for death_num in sorted_fatalities:
            sorted_fatality_counts.append(data[death_num])

        if 'sen' in chamber.lower():
            bar_color = 'red'
        elif 'rep' in chamber.lower():
            bar_color = 'blue'
        else:
            bar_color = 'green'

        graph_fatality_frequency(
            data_set=sim_data,
            sorted_fatality_counts=sorted_fatality_counts,
            sorted_fatalities=sorted_fatalities,
            x_axis_label='# {} fatalities'.format(chamber),
            y_axis_label='frequency of occurrence',
            mean_citation_text='mean fatalities: {}'.format(
                mean(chamber_fatality_counts[chamber])),
            median_citation_text='median fatalities: {}'.format(
                median(chamber_fatality_counts[chamber])),
            bar_color=bar_color,
            graph_title=('COVID-19 {} Congressional Fatality Plot ({} sims)'
                .format(chamber, len(sim_data))),
            file_name='total_{}_fatalities1.html'.format(chamber))

if __name__ == '__main__':
    main()

