#!/usr/bin/env python3
"""
Utility to test the storage code & allow for command
line storage capabilities.
"""

import os
import sys
import json
from argparse import ArgumentParser
import pprint
from uuid import uuid4
import base64
import yaml
import time
from datetime import datetime
import random
import wikipedia
from bs4 import BeautifulSoup

def parse_cmd_args():
    """ Routine to setup argparse """
    parser = ArgumentParser()
    parser.add_argument('-f', '--infection-conf-file',
                        help='file with rates of infection & outcomes',
                        required=True)
    parser.add_argument('-v', '--victim-conf-file',
                        help='file with list of victims & ages',
                        required=True)
    parser.add_argument('-i', '--infection-chance',
                        help='chance that a victim is infected',
                        type=float,
                        default=60.0)
    parser.add_argument('-c', '--count',
                        type=int,
                        help='how many simulation runs to do',
                        default=100)
    parser.add_argument('-s', '--save-updated-data',
                        help='save updated data to a new file',
                        action='store_true')
    parser.add_argument('--updated-filename',
                        help='file name to save updated file to',
                        default='updated-victim-list.tsv')
    args = parser.parse_args()
    return args

def get_rate(rate_table=None, entry=None, age=None):
    rate = 0.0
    for age_range in rate_table:
        if age_range.get('start_age') <= age <= age_range.get('end_age'):
            rate = random.uniform(
                age_range.get('{}_low'.format(entry)),
                age_range.get('{}_high'.format(entry)))
            break
    return rate

def percent_roll():
    roll = random.uniform(0.0, 100.0)
    return roll

def check_victim(
    infection_chance=None,
    rate_table=None,
    victim_age=None):
    
    status = { 
        'in_icu': False,
        'alive': True,
        'hospitalized': False,
        'infected': False,
    }

    # check if infection
    infection_roll = percent_roll()
    status['i_roll'] = '{:.2f}'.format(infection_roll)
    if infection_roll <= infection_chance:
        status['infected'] = True
        # check if in hospital
        hospital_rate = get_rate(
            entry='hospital',
            rate_table=rate_table,
            age=victim_age)
        hospital_roll = percent_roll()
        # save these truncated to save space
        status['h_rate'] = '{:.2f}'.format(hospital_rate)
        status['h_roll'] = '{:.2f}'.format(hospital_roll)
        
        # if in hospital, check result
        if hospital_roll <= hospital_rate:
            status['hospitalized'] = True
            # get chance of ICU
            icu_rate = get_rate(
                entry='icu',
                rate_table=rate_table,
                age=victim_age)
            icu_roll = percent_roll()
            # save these truncated to save space
            status['c_rate'] = '{:.2f}'.format(icu_rate)
            status['c_roll'] = '{:.2f}'.format(icu_roll)
            if icu_roll <= icu_rate:
                status['in_icu'] = True

        # get the chance of death
        fatality_rate = get_rate(
            entry='fatality',
            rate_table=rate_table,
            age=victim_age)
        fatality_roll = percent_roll()
        # save these truncated to save space
        status['f_rate'] = '{:.2f}'.format(fatality_rate)
        status['f_roll'] = '{:.2f}'.format(fatality_roll)
        # return result
        if fatality_roll <= fatality_rate:
            status['alive'] = False

    return status

def load_data(file_name=None):
    file_data = None
    if file_name.endswith('json'):
        with open(file_name, 'r') as death_file:
            file_data = json.load(death_file)
    elif file_name.endswith('csv'):
        header_data = None
        with open(file_name, 'r') as victim_file:
            file_data = []
            for line in victim_file:
                if not header_data:
                    header_data = line.strip().split(',')
                else:
                    file_data.append(
                        dict(zip(header_data, line.strip().split(','))))
    elif file_name.endswith('tsv'):
        header_data = None
        with open(file_name, 'r') as victim_file:
            file_data = []
            for line in victim_file:
                if not header_data:
                    header_data = line.strip().split('\t')
                else:
                    file_data.append(
                        dict(zip(header_data, line.strip().split('\t'))))
    else:
        print('File of unknown type: {}'.format(file_name))

    return file_data

def find_bday_on_wiki(victim_record=None):
    pol_page = None
    name = "{} {}".format(
        victim_record.get('firstName'),
        victim_record.get('lastName'))
    results = wikipedia.search(name)

    for result in results:
        if 'politician' in result.lower():
            if name in result:
                pol_page = wikipedia.page(result)

    if not pol_page:
        for result in results:
            if name == result:
                try:
                    pol_page = wikipedia.page(result)
                except:
                    continue
    if not pol_page:
        print('Unable to find record for {}'.format(name))
        print(results)

    soup = BeautifulSoup(pol_page.html(), 'lxml')
    table = soup.find("table",{"class":"infobox vcard"})
    rows = table.findChildren(['tr'])
    bday = None
    for row in rows:
        if 'born' in row.text.lower():
            cols = row.findChildren('td')
            for col in cols:
                bday_col = col.findChildren('span', {"class": "bday"})
                if bday_col:
                    break
            bday = bday_col[0].get_text()
            break
    print(name, bday)
    return bday

def calculate_age(victim_record=None):
    cur_age = None
    bday_str = None
    birthday = None
    bday_str_patterns = ["%m/%d/%Y", "%m/%d/%y", "%Y-%m-%d"]
    if victim_record.get('birthday'):
        bday_str = victim_record.get('birthday')
    else:
        bday_str = find_bday_on_wiki(victim_record=victim_record)
    for bday_str_pattern in bday_str_patterns:
        try:
            birthday = datetime.strptime(
                bday_str, bday_str_pattern)
        except Exception as e:
            pass
    
    if not birthday:
        print('Unable to figure out birthday string: {}'.format(
            bday_str))
        raise Exception
    age = datetime.now() - birthday
    cur_age = int(age.days / 365)

    return cur_age, birthday.strftime("%m/%d/%Y")

def summarize_run(run_data=None, demog_data=None, infection_chance=None):
    summary_data = {
        'fatalities': 0,
        'hospitalized': 0,
        'in_icu': 0,
        'infected': 0,
        'total_age': 0,
        'total': 0,
        'infection_chance': infection_chance,
        'party_stats': {},
        'chamber_stats': {}
    }
    for entry in run_data:
        cur_name = entry.get('name')
        cur_party = demog_data[cur_name].get('party')
        cur_chamber = demog_data[cur_name].get('chamber')
        cur_age = demog_data[cur_name].get('age')
        if cur_party not in summary_data['party_stats']:
            summary_data['party_stats'][cur_party] = {
                 'fatalities': 0,
                 'hospitalized': 0,
                 'in_icu': 0,
                 'infected': 0,
                 'total_age': 0,
                 'total': 0}
        if cur_chamber not in summary_data['chamber_stats']:
            summary_data['chamber_stats'][cur_chamber] = {
                 'fatalities': 0, 
                 'hospitalized': 0,
                 'in_icu': 0,
                 'infected': 0,
                 'total_age': 0,
                 'total': 0,
                 'party_stats': {}
                 }
        if cur_party not in summary_data['chamber_stats'][cur_chamber]['party_stats']:
            summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party] = {
                'fatalities': 0,
                'hospitalized': 0,
                'in_icu': 0,
                 'infected': 0,
                'total': 0}
        if not entry.get('alive'):
            summary_data['fatalities'] += 1
            summary_data['party_stats'][cur_party]['fatalities'] += 1
            summary_data['chamber_stats'][cur_chamber]['fatalities'] += 1
            summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party]['fatalities'] += 1
        if entry.get('hospitalized'):
            summary_data['hospitalized'] += 1
            summary_data['chamber_stats'][cur_chamber]['hospitalized'] += 1
            summary_data['party_stats'][cur_party]['hospitalized'] += 1
            summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party]['hospitalized'] += 1
        if entry.get('in_icu'):
            summary_data['in_icu'] += 1
            summary_data['chamber_stats'][cur_chamber]['in_icu'] += 1
            summary_data['party_stats'][cur_party]['in_icu'] += 1
            summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party]['in_icu'] += 1
        if entry.get('infected'):
            summary_data['infected'] += 1
            summary_data['chamber_stats'][cur_chamber]['infected'] += 1
            summary_data['party_stats'][cur_party]['infected'] += 1
            summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party]['infected'] += 1

        summary_data['total_age'] += cur_age
        summary_data['party_stats'][cur_party]['total_age'] += cur_age
        summary_data['party_stats'][cur_party]['total'] += 1
        summary_data['chamber_stats'][cur_chamber]['total_age'] += cur_age
        summary_data['chamber_stats'][cur_chamber]['total'] += 1
        summary_data['total'] += 1
        summary_data['chamber_stats'][cur_chamber]['party_stats'][cur_party]['total'] += 1
   
    avg_age = summary_data['total_age'] / summary_data['total']
    summary_data['avg_age'] = '{:.2f}'.format(avg_age)
    for party in summary_data['party_stats']:
        avg_age = summary_data['party_stats'][party]['total_age']\
            / summary_data['party_stats'][party]['total']
        summary_data['party_stats'][party]['avg_age'] = '{:.2f}'.format(avg_age)
        del(summary_data['party_stats'][party]['total_age'])
    for chamber in summary_data['chamber_stats']:
        avg_age = summary_data['chamber_stats'][chamber]['total_age']\
            / summary_data['chamber_stats'][chamber]['total'] 
        summary_data['chamber_stats'][chamber]['avg_age'] = '{:.2f}'.format(avg_age)
        del(summary_data['chamber_stats'][chamber]['total_age'])
    del(summary_data['total_age'])

    return summary_data

def save_updated_data(file_name=None):
    output_filename = file_name
    base_filename = output_filename
    prefix = 0
    print('Checking for {}'.format(output_filename))
    if os.path.exists(output_filename):
        print('File exists, making new filename')
        while os.path.exists(output_filename):
            file_parts = base_filename.split('.')
            output_filename = file_parts[0] + '_{}.'.format(prefix) + '.'.join(file_parts[1:])
            prefix += 1
    else:
        output_filename = args.updated_filename
    with open(output_filename, 'w') as new_data_file:
        headers = total_runs[0].get('run_data')[0].keys()
        new_data_file.write('\t'.join(headers) + '\n')
        for entry in total_runs[0].get('run_data'):
            out_str = ''
            for header in headers:
                out_str += (str(entry.get(header)) + '\t')
            out_str = out_str.rstrip('\t')
            out_str += '\n'
            new_data_file.write(out_str)

def save_output_data(data_file=None):
    if len(data_file):
        infect_chance = data_file[0]['summary_data']['infection_chance']
        filename = "sim_run_infect_{}_{}.json".format(
            infect_chance,
            int(time.time()))
        print('Saving output to {}'.format(filename))
        with open(filename, 'w') as out_file:
            json.dump(data_file, out_file)

def main():

    args = parse_cmd_args()
    # load death table
    infection_table = load_data(file_name=args.infection_conf_file)
    # load victim table
    victim_table = load_data(file_name=args.victim_conf_file)

    max_fatalities = 0
    min_fatalities = 100000
    total_runs = []
    count = args.count
    victim_data = {}
    while count:
        run_summary = {
            'run_data': [],
            'demog_data': {}
        }
        for victim_record in victim_table:
            victim_run = {}
            name = '{} {}'.format(
                victim_record.get('firstName'),
                victim_record.get('lastName'))
            victim_run['name'] = name
            if 'age' not in victim_record:
                age, birthday = calculate_age(victim_record=victim_record)
            else:
                age = int(victim_record['age'])
                birthday = victim_record['birthday']

            if name not in victim_data:
                victim_data = {}
                victim_data['state'] = victim_record.get('state')
                victim_data['chamber'] = victim_record.get('chamber')
                victim_data['party'] = victim_record.get('party')
                victim_data['age'] = age
                victim_data['birthday'] = birthday

            victim_run.update(check_victim(
                infection_chance=args.infection_chance,
                rate_table=infection_table.get('infection_rate_table'),
                victim_age=age))

            run_summary['run_data'].append(victim_run)
            run_summary['demog_data'][name] = victim_data


        summary_data = summarize_run(
            run_data=run_summary['run_data'],
            demog_data=run_summary['demog_data'],
            infection_chance=args.infection_chance)
        run_summary['summary_data'] = summary_data
        total_runs.append(run_summary)
        fatalities = run_summary['summary_data']['fatalities']
        if fatalities < min_fatalities:
            min_fatalities = fatalities
        if fatalities > max_fatalities:
            max_fatalities = fatalities
        sys.stdout.write("Run {:06d} complete: total fatalities = {:03d}, min = {:03d}, max = {:03d}\r".format(
            count, fatalities, min_fatalities, max_fatalities))
        sys.stdout.flush()
        count -= 1
        #pp = pprint.PrettyPrinter(indent=2)
        #pp.pprint(run_summary['summary_data'])

    #print(total_runs)
    if args.save_updated_data:
        save_updated_data(file_name=args.updated_filename)
    save_output_data(data_file=total_runs)

if __name__ == '__main__':
    main()
